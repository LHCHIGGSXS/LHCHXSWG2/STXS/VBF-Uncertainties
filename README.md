# VBF-qq2Hqq-uncertainties
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.4354861.svg)](https://doi.org/10.5281/zenodo.4354861)

A simple tool to propagate VBF uncertainties in STXS stage 1.2 bins.
The details of the calculation and the method can be found [here](https://indico.cern.ch/event/826136/contributions/3560473/attachments/1927391/3191007/STXS-uncertainties-VBF.pdf).

Numbers are extracted using the following tools:
- `proVBF  `: Phys.Rev.Lett. 117 (2016) no.7, 072001 [arXiv:1606.00840](https://arxiv.org/abs/1606.00840)
- `HJets   `: Phys.Rev.Lett. 111 (2013) 211802 [arXiv:1308.2932](https://arxiv.org/abs/1308.2932)
- `Pythia8 `: JHEP05 (2006) 026, Comput. Phys. Comm. 178 (2008) 852. [arXiv:0710.3820](https://arxiv.org/abs/0710.3820)
- `Herwig7 `: Eur.Phys.J. C76 (2016) no.4, 196 [arXiv:1512.01178](https://arxiv.org/abs/1512.01178)
- `POWHEG VBFH`: JHEP 1002 (2010) 037 [arXiv:0911.5299](https://arxiv.org/abs/0911.5299)
- `HAWK    `:  Phys.Rev.Lett. 99 (2007) 161803 [arXiv:0707.0381](https://arxiv.org/abs/0707.0381)

## How to run:
```bash
g++ -std=c++11 -o tool qq2Hqq_uncert_scheme.cpp
./tool
```

## Authors
- Claudia Bertella
- Yacine Haddad

contact:  `yhaddad[@]cern.ch`

## release notes:
- Updates 25-03-2020:
  - Adding EKW corrections using HAWK
- Updated 05-06-2020:
  - Adding s-channel contribution using HJets + Herwig 7
  - Updating acceptances for POWHEG

## Cite
 ```
@software{yacine_haddad_2020_4354861,
  author       = {Yacine Haddad and Claudia Bertella},
  title        = {yhaddad/VBF-qq2Hqq-uncertainties: v0.2},
  month        = dec,
  year         = 2020,
  publisher    = {Zenodo},
  version      = {v0.2},
  doi          = {10.5281/zenodo.4354861},
  url          = {https://doi.org/10.5281/zenodo.4354861}
}
 ```


